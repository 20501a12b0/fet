function getDifference(a, b) {
    if (b > a) {
      [a, b] = [b, a];
    }
    return Math.max(a - b, 0);
  }
  console.log(getDifference(5, 3));
  console.log(getDifference(5, 8));
  