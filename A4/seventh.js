function countPoints(scores) {
    let points = 0;
    for (let i = 0; i < scores.length; i++) {
      const [x, y] = scores[i].split(':').map(Number);
      if (x > y) {
        points += 3;
      } else if (x === y) {
        points += 1;
      }
    }
    return points;
  }
  console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']));
  